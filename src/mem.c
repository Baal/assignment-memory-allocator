#include <stdarg.h>
#define _DEFAULT_SOURCE
#include <unistd.h>
#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <assert.h>
#include <errno.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

void debug_block(struct block_header* b, const char* fmt, ... );
void debug(const char* fmt, ... );

extern inline block_size size_from_capacity( block_capacity cap );
extern inline block_capacity capacity_from_size( block_size sz );

static bool            block_is_big_enough( size_t query, struct block_header* block ) { return block->capacity.bytes >= query; }
static size_t          pages_count   ( size_t mem )                      { return mem / getpagesize() + ((mem % getpagesize()) > 0); }
static size_t          round_pages   ( size_t mem )                      { return getpagesize() * pages_count( mem ) ; }

static void block_init( void* restrict addr, block_size block_sz, void* restrict next ) {
  *((struct block_header*)addr) = (struct block_header) {
    .next = next,
    .capacity = capacity_from_size(block_sz),
    .is_free = true
  };
}

static size_t region_actual_size( size_t query ) { return size_max( round_pages( query ), REGION_MIN_SIZE ); }

extern inline bool region_is_invalid( const struct region* r );

#define MMAP_FD -1
#define MMAP_OFFSET 0

static void* map_pages(void const* addr, size_t length, int additional_flags) {
  void* ans = (void*) mmap( 
      (void*) addr, 
      length, 
      PROT_READ | PROT_WRITE, 
      MAP_PRIVATE | MAP_ANONYMOUS | additional_flags, 
      MMAP_FD, 
      MMAP_OFFSET );

  switch (errno)
  {
    case(EACCES): {fprintf(stderr, "FD acces error durring mapping pages \n"); return NULL;}
    case(EEXIST): {fprintf(stderr, "region alredy exist \n"); return NULL;}
    case(EINVAL): {fprintf(stderr, "wrong addr, length or offset \n"); return NULL;}
    case(ENFILE): {fprintf(stderr, "too many files in system \n"); return NULL;}
    case(EMFILE): {fprintf(stderr, "no more free memory \n"); return NULL;}
    case(ENOMEM): {fprintf(stderr, "insufficient memory is available \n"); return NULL;}
    case(ENXIO): {fprintf(stderr, "Addresses in the specified range are invalid for fildes \n"); return NULL;}
    default: {return ans;}
  }
}

/*  аллоцировать регион памяти*/
static struct region alloc_region  ( void const * addr, size_t query ) {
  size_t region_size = region_actual_size(query + offsetof(struct block_header, contents));
  
  fprintf(stderr, "provided addres: %zu \nprovided region_size: %zu \n", (size_t)addr, region_size);
  struct region new_region = {
     .addr = map_pages(addr, region_size, MAP_FIXED),
     .extends = true,
     .size = region_size
  };
  fprintf(stderr, "mmap addres: %zu \n", (size_t)new_region.addr);

  if (new_region.addr == MAP_FAILED || new_region.addr == NULL) 
    {fprintf(stderr, "trying any location \n"); new_region.addr = map_pages(addr, region_size, 0);}

  if (new_region.addr == MAP_FAILED || new_region.addr == NULL) fprintf(stderr, "region is invalid \n");
  fprintf(stderr, "final mmap addres: %zu \n", (size_t)new_region.addr);
  fprintf(stderr, "\n");

  return new_region;
}

static void* block_after( struct block_header const* block )         ;

/*аллоцироввать память для начала кучи и инициализировать её блоком*/
void* heap_init( size_t initial ) {
  const struct region region = alloc_region( HEAP_START, initial );
  if ( region_is_invalid(&region) ) return NULL;

  block_init(region.addr, (block_size){.bytes = region.size}, NULL);

  return region.addr;
}

#define BLOCK_MIN_CAPACITY 24

/*  --- Разделение блоков (если найденный свободный блок слишком большой )--- */

static bool block_splittable( struct block_header* restrict block, size_t query) {
  if (block == NULL) return false;
  return block-> is_free && query + offsetof( struct block_header, contents ) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

static bool split_if_too_big( struct block_header* block, size_t query ) {
  if (block_splittable(block, query) == false) return false;

  fprintf(stderr, "splitting block\n");
  fprintf(stderr, "initial first block capacity %zu\n", block->capacity.bytes);

  block_capacity initial_block_capacity = block -> capacity;
  block_capacity first_block_capacity = {.bytes = query};
  block->capacity = first_block_capacity;
  fprintf(stderr, "fist block capacity has been changed to %zu\n", block->capacity.bytes);
  
  struct block_header* second = (struct block_header*) block_after(block);
  block_init(block_after(block), (block_size){.bytes = (initial_block_capacity.bytes) - (query + offsetof(struct block_header, contents))}, block->next);
  fprintf(stderr, "second block has been initialized with capacity %zu\n", second -> capacity.bytes);

  block -> next = second;

  fprintf(stderr, "\n");

  return true;
}


/*  --- Слияние соседних свободных блоков --- */

static void* block_after( struct block_header const* block )              {
  return  (void*) (block->contents + block->capacity.bytes);
}
static bool blocks_continuous (
                               struct block_header const* fst,
                               struct block_header const* snd ) {
  return (void*)snd == block_after(fst);
}

static bool mergeable(struct block_header const* restrict fst, struct block_header const* restrict snd) {
  return fst->is_free && snd->is_free && blocks_continuous( fst, snd ) ;
}

static bool try_merge_with_next( struct block_header* block ) {
  if (block == NULL) return false;
  struct block_header* next_block = block -> next;
  if (next_block == NULL) return false;
  if (mergeable(block, next_block) == false) return false;

  (block -> capacity).bytes = (block->capacity).bytes + ((next_block->capacity).bytes) + offsetof(struct block_header, contents);
  block -> next = next_block -> next;
  return true;
}


/*  --- ... ecли размера кучи хватает --- */

struct block_search_result {
  enum {BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED} type;
  struct block_header* block;
};


static struct block_search_result find_good_or_last  ( struct block_header* restrict block, size_t sz )    {
  if (block == NULL) return (struct block_search_result) {.block = NULL, .type = BSR_CORRUPTED};

  fprintf(stderr, "trying to find good or last \n");

  while (block != NULL){
    while (try_merge_with_next(block));

    if (block_is_big_enough(sz, block) && block->is_free)
      return (struct block_search_result) {.type = BSR_FOUND_GOOD_BLOCK, .block = block};

    fprintf(stderr, "going to the next block \n");

    if (block->next == NULL)
      return (struct block_search_result) {.type = BSR_REACHED_END_NOT_FOUND, .block = block};
    block = block->next;
  }

  return (struct block_search_result) {.type = BSR_CORRUPTED, .block = NULL};
}

/*  Попробовать выделить память в куче начиная с блока `block` не пытаясь расширить кучу
 Можно переиспользовать как только кучу расширили. */
static struct block_search_result try_memalloc_existing ( size_t query, struct block_header* block ) {
  struct block_search_result result = find_good_or_last(block, query);

  split_if_too_big(result.block, query);
  return result;
}


/* Сначала надо попытаться выделить память вплотную к концу кучи и разметить её в один большой свободный блок. Если в первом регионе кучи последний блок был свободен, надо объединить его со следующим.
  Если выделить регион вплотную не получилось, то нужно выделить регион "где получится". Последний блок предыдущего региона будет связан с первым блоком нового региона.*/
static struct block_header* grow_heap( struct block_header* restrict last, size_t query ) {
  struct region new_region = alloc_region(last, query);

  if (region_is_invalid(&new_region)) return NULL;

  struct block_header* new_header = new_region.addr;
  block_init(new_header, (block_size){.bytes = new_region.size}, NULL);
  last->next = new_header;

  if (try_merge_with_next(last)) return last;
  return new_header;
}

static bool HEAP_NOT_INITILIZED = true;
/*  Реализует основную логику malloc и возвращает заголовок выделенного блока */
static struct block_header* memalloc( size_t query, struct block_header* heap_start) {
  if (query < BLOCK_MIN_CAPACITY) query = BLOCK_MIN_CAPACITY;
  if (HEAP_NOT_INITILIZED){
    fprintf(stderr, "initializing heap \n");
    heap_init(query);
    HEAP_NOT_INITILIZED = false;
  }

  struct block_search_result result = try_memalloc_existing(query, heap_start);

  switch(result.type){
    case BSR_FOUND_GOOD_BLOCK: {
      fprintf(stderr, "found good block before growing heap of size %zu\n\n", result.block->capacity.bytes);
      result.block->is_free = false; 
      return result.block;
    }
    case BSR_REACHED_END_NOT_FOUND: {
      fprintf(stderr, "\nprovided last block addres: %zu\nprovided block capacity: %zu\nprovided block after addres: %zu \n \n",
        (size_t)result.block,(size_t)(result.block->capacity.bytes),(size_t)block_after(result.block));
      grow_heap(block_after(result.block), query);

      struct block_search_result new_result = try_memalloc_existing(query, heap_start);

      switch(new_result.type){
        case BSR_FOUND_GOOD_BLOCK: {
          fprintf(stderr, "\nfound good block after growing heap of size %zu\n\n", result.block->capacity.bytes); 
          result.block->is_free = false;
          return new_result.block;}
        case BSR_REACHED_END_NOT_FOUND: {return NULL;}
        case BSR_CORRUPTED: {fprintf(stderr, "block search corrupted, NULL is not valid block addres \n"); return NULL;}
      }

      return NULL;
    }
    case BSR_CORRUPTED: {fprintf(stderr, "block search corrupted, NULL is not valid block addres \n"); return NULL;}
  }

  return NULL;
}

void* _malloc( size_t query ) {
  struct block_header* const addr = memalloc( query, (struct block_header*) HEAP_START );
  if (addr) return addr->contents;
  else return NULL;
}

static struct block_header* block_get_header(void* contents) {
  return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}

/*Если addr == NULL, то не делаем ничего.
Нам нужно получить из addr (который указывает на начало поля contents) адрес начала заголовка (для этого вычтем из него sizeof(struct mem)).
В заголовке блока установим is_free = true, всё.
Помимо уже написанного про free, при освобождении блока можно объединить его со всеми следующими за ним свободными блоками.*/
void _free( void* mem ) {
  if (!mem) return ;
  struct block_header* header = block_get_header( mem );
  header->is_free = true;
  
  while (try_merge_with_next(header));
}
