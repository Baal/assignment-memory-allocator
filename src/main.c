#include <mem.h>
#include <util.h>
#include <mem_internals.h>

#ifndef MAP_FIXED
#define MAP_FIXED 0x100000
#endif
#ifndef MAP_ANONYMOUS
#define MAP_ANONYMOUS 0x20
#endif

#define SMALL_BLOCK_REQUEST_SIZE 25
#define ENORMOUS_BLOCK_REQUEST_SIZE 2*4096

static struct block_header* get_block_header (void* data){
    return (struct block_header*)(((uint8_t*)data)-offsetof(struct block_header, contents));
}

int test1(void){
    void* result = _malloc(SMALL_BLOCK_REQUEST_SIZE);
    fprintf(stderr, "\nfirst test: _malloc completed \n");

    return result != NULL;
}

int test2(void){
    void* addr1 = _malloc(SMALL_BLOCK_REQUEST_SIZE);
    void* addr2 = _malloc(SMALL_BLOCK_REQUEST_SIZE);
    void* addr3 = _malloc(SMALL_BLOCK_REQUEST_SIZE);
    fprintf(stderr, "\nsecond test: first _malloc completed \n");

    _free(addr1);
    fprintf(stderr, "\nsecond test: _free completed \n");

    void* addr4 = _malloc(SMALL_BLOCK_REQUEST_SIZE);
    fprintf(stderr, "\nsecond test: second _malloc completed \n");

    return addr2 == NULL && addr3 != NULL && addr4 != NULL;
}

int test3(void){
    void* addr1 = _malloc(SMALL_BLOCK_REQUEST_SIZE);
    void* addr2 = _malloc(ENORMOUS_BLOCK_REQUEST_SIZE);
    void* addr3 = _malloc(SMALL_BLOCK_REQUEST_SIZE);
    fprintf(stderr, "\nthird test: first _malloc completed \n");

    _free(addr1);
    _free(addr3);
    fprintf(stderr, "\nthird test: _free completed \n");

    return addr1 != NULL && addr2 == NULL && addr3 != NULL;
}

int test4(void){
    void* addr1 = _malloc(ENORMOUS_BLOCK_REQUEST_SIZE);
    void* addr2 = _malloc(ENORMOUS_BLOCK_REQUEST_SIZE);
    void* addr3 = _malloc(ENORMOUS_BLOCK_REQUEST_SIZE);

    return addr1 != NULL && addr2 != NULL && addr3 != NULL;
}

int test5(void){
    void* addr1 = _malloc(ENORMOUS_BLOCK_REQUEST_SIZE);

    struct block_header* first = get_block_header(addr1);
    while((first -> next) != NULL) first = first -> next;

    void* heap_end = (first->capacity).bytes + first->contents;
    mmap(heap_end, ENORMOUS_BLOCK_REQUEST_SIZE, 
        PROT_READ | PROT_WRITE | PROT_EXEC, 
        MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED, 
        0, 0);

    void* addr2 = _malloc(ENORMOUS_BLOCK_REQUEST_SIZE);

    return addr1 != NULL && addr2 != NULL;
}

int main(){
    printf("first   test result: \n%d\n\n",     test1());
    printf("second  test result: \n%d\n\n",     test2());
    printf("third   test result: \n%d\n\n",     test3());
    printf("forth   test result: \n%d\n\n",     test4());
    printf("fifth   test result: \n%d\n\n",     test5());

    return 0;
}
